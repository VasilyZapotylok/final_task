package by.epam.shop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.shop.entity.Product;
import by.epam.shop.exception.DAOException;


public class ProductDAO extends AbstractDAO<Product> {
	
	private static final String SQL_SELECT_PRODUCT = "SELECT products.id, product_types_id, product_pictures_id, producer, model, size, pcd, dia, et, products.description, price, product_types.id, product_types.description, product_pictures.id, product_pictures.path FROM store.products JOIN store.product_types ON (products.product_types_id = product_types.id) JOIN store.product_pictures ON (products.product_pictures_id = product_pictures.id)";
	private static final String SQL_SELECT_PRODUCT_BY_ID = "SELECT products.id, product_types_id, product_pictures_id, producer, model, size, pcd, dia, et, products.description, price, product_types.id, product_types.description, product_pictures.id, product_pictures.path FROM store.products JOIN store.product_types ON (products.product_types_id = product_types.id) JOIN store.product_pictures ON (products.product_pictures_id = product_pictures.id) WHERE products.id= ?";
	private static final String SQL_SELECT_PRODUCT_BY_TYPE = "SELECT products.id, product_types_id, product_pictures_id, producer, model, size, pcd, dia, et, products.description, price, product_types.id, product_types.description, product_pictures.id, product_pictures.path FROM store.products JOIN store.product_types ON (products.product_types_id = product_types.id) JOIN store.product_pictures ON (products.product_pictures_id = product_pictures.id) WHERE product_types.description= ?";
	private static final String SQL_CREATE_PRODUCT = "INSERT INTO store.products (product_types_id, product_pictures_id, producer, model, size, pcd, dia, et, description, price) VALUES (?,?,?,?,?,?,?,?,?,?)";
	private static final String SQL_UPDATE_PRODUCT = "UPDATE store.products SET product_types_id= ?, product_pictures_id= ?, producer=?, model=?, size=?, pcd=?, dia=?, et=?, description= ?, price= ? WHERE id= ?";
	private static final String SQL_DELETE_PRODUCT = "DELETE FROM store.products WHERE id= ?";
	private static final String SQL_DELETE_ORDERS_PRODUCTS = "DELETE FROM store.orders_products WHERE products_id= ?";
	private static final String SQL_SELECT_PICTURE = "SELECT id, path FROM store.product_pictures WHERE path= ?";
	private static final String SQL_SELECT_TYPE = "SELECT id, description FROM store.product_types WHERE description= ?";
	private static final String SQL_CHECK_ACTIVE_ORDER = "SELECT products.id, product_types_id, product_pictures_id, producer, model, size, pcd, dia, et, products.description, price, orders_products.id, orders_products.orders_id, orders_products.products_id, orders.id, orders.users_id, orders.status_id, orders.date, orders.comment FROM store.products JOIN store.orders_products ON (products.id = orders_products.products_id) JOIN store.orders ON (orders.id = orders_products.orders_id) WHERE orders.status_id = 1 AND products.id= ?";
	private static final String SQL_SELECT_PRODUCT_TYPES = "SELECT id, description FROM store.product_types";
	private static final String SQL_SELECT_PRODUCT_PICTURES = "SELECT id, path FROM store.product_pictures";

	
	@Override
	public List<Product> findAll() throws DAOException {
		ArrayList<Product> products = new ArrayList<>();
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PRODUCT);
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				Product product = new Product();
				product.setId(resultSet.getInt("products.id"));
				product.setType(resultSet.getString("product_types.description"));
				product.setPicturePath(resultSet.getString("product_pictures.path"));
				product.setProducer(resultSet.getString("products.producer"));
				product.setModel(resultSet.getString("products.model"));
				product.setSize(resultSet.getString("products.size"));
				product.setPcd(resultSet.getString("products.pcd"));
				product.setDia(resultSet.getDouble("products.dia"));
				product.setEt(resultSet.getInt("products.et"));
				product.setDescription(resultSet.getString("products.description"));
				product.setPrice(resultSet.getInt("products.price"));
				products.add(product);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return products;
	}


	@Override
	public Product findEntityById(Integer id) throws DAOException {
		Product product = null;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PRODUCT_BY_ID);
			prepareStatement.setInt(1, id);
			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				product = new Product();
				product.setId(resultSet.getInt("products.id"));
				product.setType(resultSet.getString("product_types.description"));
				product.setPicturePath(resultSet.getString("product_pictures.path"));
				product.setProducer(resultSet.getString("products.producer"));
				product.setModel(resultSet.getString("products.model"));
				product.setSize(resultSet.getString("products.size"));
				product.setPcd(resultSet.getString("products.pcd"));
				product.setDia(resultSet.getDouble("products.dia"));
				product.setEt(resultSet.getInt("products.et"));
				product.setDescription(resultSet.getString("products.description"));
				product.setPrice(resultSet.getInt("products.price"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return product;
	}


	public List<Product> findEntitiesByType(String type) throws DAOException {
		ArrayList<Product> products = new ArrayList<>();
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PRODUCT_BY_TYPE);
			prepareStatement.setString(1, type);
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				Product product = new Product();
				product.setId(resultSet.getInt("products.id"));
				product.setType(resultSet.getString("product_types.description"));
				product.setPicturePath(resultSet.getString("product_pictures.path"));
				product.setProducer(resultSet.getString("products.producer"));
				product.setModel(resultSet.getString("products.model"));
				product.setSize(resultSet.getString("products.size"));
				product.setPcd(resultSet.getString("products.pcd"));
				product.setDia(resultSet.getDouble("products.dia"));
				product.setEt(resultSet.getInt("products.et"));
				product.setDescription(resultSet.getString("products.description"));
				product.setPrice(resultSet.getInt("products.price"));
				products.add(product);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return products;
	}


	@Override
	public boolean delete(Integer id) throws DAOException {
		if (checkActiveOrder(id)) {
			return false;
		}
		boolean flag = false;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
		    prepareStatement = connection.prepareStatement(SQL_DELETE_PRODUCT);
			deleteFromOrdersProducts(id);
			prepareStatement.setInt(1, id);
			int count = prepareStatement.executeUpdate();
			if (count == 1) {
				flag = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return flag;
	}

	@Override
	public boolean delete(Product entity) throws DAOException {
		if (checkActiveOrder(entity.getId())) {
			return false;
		}
		boolean flag = false;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_DELETE_PRODUCT);
			deleteFromOrdersProducts(entity.getId());
			prepareStatement.setInt(1, entity.getId());
			int count = prepareStatement.executeUpdate();
			if (count == 1) {
				flag = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return flag;
	}


	@Override
	public boolean create(Product entity) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			int typeId = findTypeId(entity.getType());
			int pictureId = findPictureId(entity.getPicturePath());
			prepareStatement = connection.prepareStatement(SQL_CREATE_PRODUCT);
			prepareStatement.setInt(1, typeId);
			prepareStatement.setInt(2, pictureId);
			prepareStatement.setString(3, entity.getProducer());
			prepareStatement.setString(4, entity.getModel());
			prepareStatement.setString(5, entity.getSize());
			prepareStatement.setString(6, entity.getPcd());
			prepareStatement.setDouble(7, entity.getDia());
			prepareStatement.setInt(8, entity.getEt());
			prepareStatement.setString(9, entity.getDescription());
			prepareStatement.setInt(10, entity.getPrice());
			int count = prepareStatement.executeUpdate();
			if (count == 1) {
				flag = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return flag;
	}

	@Override
	public Product update(Product entity) throws DAOException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			int typeId = findTypeId(entity.getType());
			int pictureId = findPictureId(entity.getPicturePath());
			prepareStatement = connection.prepareStatement(SQL_UPDATE_PRODUCT);
			prepareStatement.setInt(1, typeId);
			prepareStatement.setInt(2, pictureId);
			prepareStatement.setString(3, entity.getProducer());
			prepareStatement.setString(4, entity.getModel());
			prepareStatement.setString(5, entity.getSize());
			prepareStatement.setString(6, entity.getPcd());
			prepareStatement.setDouble(7, entity.getDia());
			prepareStatement.setInt(8, entity.getEt());
			prepareStatement.setString(9, entity.getDescription());
			prepareStatement.setInt(10, entity.getPrice());
			prepareStatement.setInt(11, entity.getId());
			int count = prepareStatement.executeUpdate();
			if (count == 1) {
				return entity;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return null;
	}


	public List<String> findAllProductTypes() throws DAOException {
		ArrayList<String> types = new ArrayList<>();
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PRODUCT_TYPES);
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				types.add(resultSet.getString("description"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return types;
	}


	public List<String> findAllProductPicturePath() throws DAOException {
		ArrayList<String> path = new ArrayList<>();
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PRODUCT_PICTURES);
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				path.add(resultSet.getString("path"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return path;
	}


	private boolean checkActiveOrder(int id) throws DAOException {
		boolean flag = false;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_CHECK_ACTIVE_ORDER);
			prepareStatement.setInt(1, id);
			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				flag = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return flag;
	}


	private Integer findTypeId(String type) throws DAOException {
		Integer id = null;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_TYPE);
			prepareStatement.setString(1, type);
			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				id = new Integer(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return id;
	}


	private Integer findPictureId(String path) throws DAOException {
		Integer id = null;
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_SELECT_PICTURE);
			prepareStatement.setString(1, path);
			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				id = new Integer(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
		return id;
	}


	private void deleteFromOrdersProducts(int productId) throws DAOException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement prepareStatement = null;
		try  {
			prepareStatement = connection.prepareStatement(SQL_DELETE_ORDERS_PRODUCTS);
			prepareStatement.setInt(1, productId);
			prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(prepareStatement);
			connectionPool.freeConnection(connection);
		}
	}
	public static ProductDAO getInstance(){
		return new ProductDAO();
	}
}
