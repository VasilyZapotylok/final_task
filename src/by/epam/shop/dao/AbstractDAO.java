package by.epam.shop.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.shop.connectionpool.ConnectionPool;
import by.epam.shop.entity.AbstractEntity;
import by.epam.shop.exception.DAOException;

/**
 * The base abstract class for all DAO in the project. 
 */

public abstract class AbstractDAO<T extends AbstractEntity> {
	
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class);
	
	protected static ConnectionPool connectionPool = ConnectionPool.getInstance();
	
	public abstract List<T> findAll() throws DAOException;

	public abstract T findEntityById(Integer id) throws DAOException;

	public abstract boolean delete(Integer id) throws DAOException;

	public abstract boolean delete(T entity) throws DAOException;

	public abstract boolean create(T entity) throws DAOException;
	
	public abstract T update(T entity) throws DAOException;

    public void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error("Error close prepared statement", e);
            }
        }
    }


}
