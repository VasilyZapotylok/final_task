package by.epam.shop.connectionpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.mysql.jdbc.Driver;

/**
 * The class ConnectionPool contains connections to the database.
 */

public class ConnectionPool {
	private static final String PROPERTY_FILE = "connection_pool";
	private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
	private String user;
	private String password;
	private String url;
	private ArrayBlockingQueue<Connection> connectionPool;
	private static int poolSize;
	private static ConnectionPool instance;
	private static AtomicBoolean isCreated = new AtomicBoolean(true);
	private static ReentrantLock lock = new ReentrantLock();

	private ConnectionPool() {
		try {
			DriverManager.registerDriver(new Driver());
			ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);
			poolSize = Integer.parseInt(resourceBundle.getString("pool_size"));
			user = resourceBundle.getString("user");
			password = resourceBundle.getString("password");
			url = resourceBundle.getString("url");
			connectionPool = new ArrayBlockingQueue<>(poolSize);
			for (int i = 0; i < poolSize; i++) {
				connectionPool.put(DriverManager.getConnection(url, user, password));
			}
		} catch (SQLException | InterruptedException e) {
			LOGGER.fatal(e);
			throw new ExceptionInInitializerError(e);
		}
	}

	/**
	 * The method gets a single instance of ConnectionPool.
	 */
	public static ConnectionPool getInstance() {
		if (isCreated.get()) {
			lock.lock();
			try {
				if (isCreated.get()) {
					instance = new ConnectionPool();
					isCreated.set(false);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	/**
	 * The method provides connections for users.
	 */
	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = connectionPool.take();
		} catch (InterruptedException e) {
			LOGGER.error(e);
		}
		return connection;
	}

	/**
	 * The method returns connection to the connection pool.
	 */
	public void freeConnection(Connection connection) {
		try {
			if (!connection.isClosed()) {
				connectionPool.put(connection);
			}
		} catch (SQLException | InterruptedException e) {
			LOGGER.error(e);
		}
	}

	/**
	 * The method closes all connections in the connection pool.
	 */
	public void shutDown() {
		Iterator<Connection> iterator = connectionPool.iterator();
		while (iterator.hasNext()) {
			try {
				iterator.next().close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
	}
}
