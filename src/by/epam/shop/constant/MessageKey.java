package by.epam.shop.constant;

/**
 * Constants for defining messages on web-pages.
 */
public class MessageKey {
	public static final String LOG_IN_ERROR = "message.loginerror";
	public static final String NULL_PAGE = "message.nullpage";
	public static final String WRONG_ACTION = "message.wrongaction";
	public static final String REGISTER_PASSWORD_MISMATCH = "message.register.password_mismatch";
	public static final String REGISTER_LOGIN_ERROR = "message.register.loginerror";
	public static final String REGISTER_BLANK_FIELDS = "message.register.blankfields";
	public static final String REGISTER_LOGIN_PATTERN_ERROR = "message.register.loginpatternerror";
	public static final String REGISTER_PASSWORD_PATTERN_ERROR = "message.register.passwordpatternerror";
	public static final String REGISTER_PHONE_PATTERN_ERROR = "message.register.phonepatternerror";
	public static final String FIND_PRODUCTS_ERROR = "message.find.products.error";
	public static final String FIND_PRODUCT_ERROR = "message.find.product.error";
	public static final String FIND_ORDER_ERROR = "message.find.order.error";
	public static final String FIND_USER_ERROR = "message.find.order.error";
	public static final String USER_ERROR = "message.user.error";
	public static final String ADD_TO_SHOPPING_CART_SUCCESS = "message.addproduct.success";
	public static final String REMOVE_FROM_SHOPPING_CART_SUCCESS = "message.removeproduct.success";
	public static final String MAKE_ORDER_ERROR = "message.makeorder.error";
	public static final String MAKE_ORDER_SUCCESS = "message.makeorder.success";
	public static final String SHOW_CART_ERROR = "message.cart.error";
	public static final String CANCEL_ORDER_ERROR = "message.cancelorder.error";
	public static final String CANCEL_ORDER_SUCCESS = "message.cancelorder.success";
	public static final String REGISTER_ERROR = "message.register.error";
	public static final String REGISTER_SUCCESS = "message.register.success";
	public static final String SHOW_ORDERS_ERROR = "message.showorder.error";
	public static final String ACESS_LEVEL_ERROR = "message.accesslevel.error";
	public static final String ADD_PRODUCT_BLANK_FIELDS = "message.addproduct.field";
	public static final String ADD_PRODUCT_SUCCESS = "message.addproduct.success";
	public static final String ADD_PRODUCT_ERROR = "message.addproduct.error";
	public static final String DELETE_ORDER_SUCCESS = "message.deleteorder.success";
	public static final String DELETE_ORDER_ERROR = "message.deleteorder.error";
	public static final String DELIVER_ORDER_SUCCESS = "message.deliverorder.success";
	public static final String DELIVER_ORDER_ERROR = "message.deliverorder.error";
	public static final String BUY_ERROR = "message.buy.error";
	public static final String DELETE_PRODUCT_SUCCESS = "message.deleteproduct.success";
	public static final String DELETE_PRODUCT_ERROR = "message.deleteproduct.error";
	public static final String EDIT_PRODUCT_SUCCESS = "message.editproduct.success";
	public static final String EDIT_PRODUCT_ERROR = "message.editproduct.error";
	public static final String DATABASE_ERROR = "message.database.error";
	public static final String PRODUCT_PRODUCER_PATTERN_ERROR = "message.product.producer.error";
	public static final String PRODUCT_MODEL_PATTERN_ERROR = "message.product.model.error";
	public static final String PRODUCT_SIZE_PATTERN_ERROR = "message.product.size.error";
	public static final String PRODUCT_PCD_PATTERN_ERROR = "message.product.pcd.error";
	public static final String PRODUCT_DIA_PATTERN_ERROR = "message.product.dia.error";
	public static final String PRODUCT_ET_PATTERN_ERROR = "message.product.et.error";
	public static final String PRODUCT_PRICE_PATTERN_ERROR = "message.product.price.error";
	public static final String PAGE_NOT_FOUD = "message.page.notfound";
}
