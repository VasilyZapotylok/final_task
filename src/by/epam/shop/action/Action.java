package by.epam.shop.action;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.manager.ConfigurationManager;

/**
 * The interface Action. Base interface for all actions used in the project.
 * Contains a method String execute(HttpServletRequest request) for return path
 * of the called web-page.
 */

public interface Action {

	static ConfigurationManager configurationManager = ConfigurationManager.getInstance();

	String execute(HttpServletRequest request);
}
