package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class RegisterAction implements Action {

	/**
	 * The method for adding a new user to the database. New user automatically
	 * gets access.
	 */

	@Override
	public String execute(HttpServletRequest request) {

		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String passwordRepeat = request.getParameter("password_repeat");
		String phone = request.getParameter("phone");

		if (login.length() * password.length() * passwordRepeat.length() * phone.length() == 0) {
			request.setAttribute("message", MessageKey.REGISTER_BLANK_FIELDS);
			return configurationManager.getProperty("path.page.register");
		}
		if (!password.equals(passwordRepeat)) {
			request.setAttribute("message", MessageKey.REGISTER_PASSWORD_MISMATCH);
			return configurationManager.getProperty("path.page.register");
		}
		UserService userService = UserService.getInstance();
		User user = userService.initUserForRegister(login, password, phone);
		String validationMessage = UserService.validateUser(user);
		if (validationMessage != null) {
			request.setAttribute("message", validationMessage);
			return configurationManager.getProperty("path.page.register");
		}
		if (userService.findUserByLogin(login) != null) {
			request.setAttribute("message", MessageKey.REGISTER_LOGIN_ERROR);
			return configurationManager.getProperty("path.page.register");
		}
		user.setPassword(UserService.hashMD5(password));
		if (userService.addUser(user)) {
			user = userService.findUserByLogin(user.getLogin());
			request.getSession().setAttribute("user", user);
			request.setAttribute("message", MessageKey.REGISTER_SUCCESS);
			return configurationManager.getProperty("path.page.success");
		} else {
			request.setAttribute("message", MessageKey.REGISTER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}
}
