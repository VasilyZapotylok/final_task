package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class RemoveFromShoppingCartAction implements Action {
	
	/**
	 * The method for removing an one concrete product from a shopping cart.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int id = Integer.parseInt(request.getParameter("product_id"));
		UserService.getInstance().deleteProductFromCart(user, id);
		request.getSession().setAttribute("user", user);
		request.setAttribute("message", MessageKey.REMOVE_FROM_SHOPPING_CART_SUCCESS);
		return configurationManager.getProperty("path.page.success");
	}

}
