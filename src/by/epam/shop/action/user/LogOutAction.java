package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;


public class LogOutAction implements Action {
	
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().removeAttribute("user");
		return configurationManager.getProperty("path.page.index");
	}
}
