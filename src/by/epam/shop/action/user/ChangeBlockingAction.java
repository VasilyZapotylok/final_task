package by.epam.shop.action.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class ChangeBlockingAction implements Action {

	/**
	 * The method for blocking a concrete user. Blocked user can't buy products.
	 */
	
	@Override
	public String execute(HttpServletRequest request) {
		User sessionUser = (User) request.getSession().getAttribute("user");
		if (sessionUser == null || sessionUser.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int userId = Integer.parseInt(request.getParameter("user_id"));
		UserService userService = UserService.getInstance();
		User user = userService.findUserById(userId);
		if (user == null) {
			request.setAttribute("message", MessageKey.FIND_USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (user.getBlackListFlag() == 0) {
			user.setBlackListFlag(1);
		} else {
			user.setBlackListFlag(0);
		}
		userService.updateUser(user);
		if (user.getId().equals(sessionUser.getId())) {
			request.getSession().setAttribute("user", user);
		}
		List<User> users = userService.findAllUsers();
		if (users != null) {
			request.setAttribute("users", users);
			return configurationManager.getProperty("path.page.users");
		} else {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}
}
