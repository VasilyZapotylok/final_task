package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class LogInAction implements Action {
	
	/**
	 * The method for getting access to the site.  
	 */

	@Override
	public String execute(HttpServletRequest request) {

		String login = request.getParameter("login");
		String password = UserService.hashMD5(request.getParameter("password"));

		User user = UserService.getInstance().findUserByLoginAndPassword(login, password);
		if (user == null) {
			request.setAttribute("message", MessageKey.LOG_IN_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			request.getSession().setAttribute("user", user);
			return configurationManager.getProperty("path.page.index");
		}
	}
}
