package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.service.product.ProductService;

public class AddToShoppingCartAction implements Action {

	/**
	 * The method for adding a concrete product to the shopping cart. Logic
	 * defined in the ProductService class.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		int amount = Integer.parseInt(request.getParameter("amount"));
		if (user == null) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (user.getBlackListFlag() == 1) {
			request.setAttribute("message", MessageKey.BUY_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int id = Integer.parseInt(request.getParameter("product_id"));
		ProductService productService = ProductService.getInstance();
		Product product = productService.findProductById(id);
		if (product == null) {
			request.setAttribute("message", MessageKey.FIND_PRODUCT_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			user = productService.checkAmount(user, product, amount);
			request.getSession().setAttribute("user", user);
			request.setAttribute("message", MessageKey.ADD_TO_SHOPPING_CART_SUCCESS);
			return configurationManager.getProperty("path.page.success");

		}

	}
}
