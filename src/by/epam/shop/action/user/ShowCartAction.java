package by.epam.shop.action.user;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class ShowCartAction implements Action {
  
	/**
	 * The method for showing a description of a shopping cart of a concrete user. 
	 */
	
	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (user.getShoppingCart().isEmpty()) {
			request.setAttribute("message", MessageKey.SHOW_CART_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int fullPrice = UserService.getInstance().countFullPrice(user);
		request.setAttribute("cart", user.getShoppingCart());
		request.setAttribute("full_price", fullPrice);
		return configurationManager.getProperty("path.page.cart");
	}

}
