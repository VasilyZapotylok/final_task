package by.epam.shop.action.locale;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;


public class ChangeLocaleAction implements Action {
	
/**
 * The method of changing the locale in the session.  
 */
	
	@Override
	public String execute(HttpServletRequest request) {
		String lang = request.getParameter("locale");
		request.getSession().setAttribute("locale", lang);
		return configurationManager.getProperty("path.page.index");
	}
}
