package by.epam.shop.action;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.constant.MessageKey;

/**
 * The class ActionFactory.
 */

public class ActionFactory {
	private static ActionFactory instance = new ActionFactory();

	private ActionFactory() {
	}

	public static ActionFactory getInstance() {
		return instance;
	}

	/**
	 * The method defineCommand(HttpServletRequest request). Returns an action
	 * object, using data from request and ActionEnum. Can return null, that
	 * will be checked in the Controller.
	 */

	public Action defineCommand(HttpServletRequest request) {
		Action current = null;
		String action = request.getParameter("action");
		if (action == null || action.isEmpty()) {
			return current;
		}
		try {
			ActionEnum currentEnum = ActionEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentAction();
		} catch (IllegalArgumentException e) {
			request.setAttribute("wrongAction", action + MessageKey.WRONG_ACTION);
		}
		return current;
	}
}
