package by.epam.shop.action.page;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.product.ProductService;

public class AddProductPageAction implements Action {

	/**
	 * The method for preparing of the page for adding a new product. Returns
	 * all product's types and picture's paths from the database.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		ProductService productService = ProductService.getInstance();
		List<String> types = productService.findAllProductTypes();
		List<String> picturePath = productService.findAllProductPicturePath();
		if (types != null & picturePath != null) {
			request.setAttribute("types", types);
			request.setAttribute("picturePath", picturePath);
			return configurationManager.getProperty("path.page.addproduct");
		} else {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
	}
}
