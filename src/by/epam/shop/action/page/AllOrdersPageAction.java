package by.epam.shop.action.page;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.User;
import by.epam.shop.service.order.OrderService;

public class AllOrdersPageAction implements Action {

	/**
	 * The method for preparing of the page with a description of all orders.
	 * Returns all orders from the database for all users.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.CANCEL_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		List<Order> orders = OrderService.getInstance().findAllOrders();
		if (orders != null) {
			request.setAttribute("orders", orders);
			return configurationManager.getProperty("path.page.allorders");
		} else {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
	}

}
