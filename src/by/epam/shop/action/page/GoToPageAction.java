package by.epam.shop.action.page;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;

public class GoToPageAction implements Action {
	/**
	 * The method for returning a called page. 
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = request.getParameter("page");
		String code = request.getParameter("code");
		request.setAttribute("code", code);
		return configurationManager.getProperty(page);
	}
}
