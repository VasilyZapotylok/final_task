package by.epam.shop.action.page;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.user.UserService;

public class UsersPageAction implements Action {

	/**
	 * The method for preparing of the page with description of the all users.
	 * Returns all user's data from the database.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.CANCEL_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		List<User> users = UserService.getInstance().findAllUsers();
		if (users != null) {
			request.setAttribute("users", users);
			return configurationManager.getProperty("path.page.users");
		} else {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
	}
}
