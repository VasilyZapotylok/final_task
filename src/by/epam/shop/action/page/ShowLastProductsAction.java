package by.epam.shop.action.page;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.service.product.ProductService;

public class ShowLastProductsAction implements Action {

	public String execute(HttpServletRequest request) {

		/**
		 * The method for preparing of the main page. Returns only new products
		 * from the database.
		 */

		ProductService productService = ProductService.getInstance();
		List<Product> products = productService.findAllProducts();
		if (products.isEmpty()) {
			request.setAttribute("message", MessageKey.FIND_PRODUCTS_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			productService.sortListForLastProducts(products);
			request.setAttribute("products", products);
			return configurationManager.getProperty("path.page.main");
		}

	}

}
