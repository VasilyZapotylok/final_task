package by.epam.shop.action.page;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.service.product.ProductService;

public class EditProductPageAction implements Action {

	/**
	 * The method for preparing of the page for editing a concrete product.
	 * Returns all product's types and picture's paths from data base and the
	 * data of the concrete product.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.CANCEL_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int productId = Integer.parseInt(request.getParameter("product_id"));
		ProductService productSrvice = ProductService.getInstance();
		Product product = productSrvice.findProductById(productId);
		List<String> types = productSrvice.findAllProductTypes();
		List<String> picturePath = productSrvice.findAllProductPicturePath();
		if (product != null & types != null & picturePath != null) {
			request.setAttribute("types", types);
			request.setAttribute("picturePath", picturePath);
			request.setAttribute("product", product);
			return configurationManager.getProperty("path.page.editproduct");
		} else {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
	}
}
