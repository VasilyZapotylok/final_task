package by.epam.shop.action.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.service.product.ProductService;

public class SearchProductAction implements Action {

	/**
	 * The method for searching products by parameters. The logic defined in the
	 * ProductService class.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		String text = request.getParameter("text_search");
		List<Product> findProducts = ProductService.getInstance().searchProducts(text);
		if (findProducts.isEmpty()) {
			request.setAttribute("message", MessageKey.FIND_PRODUCTS_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			request.setAttribute("products", findProducts);
			return configurationManager.getProperty("path.page.products");
		}
	}
}
