package by.epam.shop.action.product;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.service.product.ProductService;

public class ShowConcreteProductAction implements Action {
	
	/** 
	 * The method for showing description of a concrete product from the database. 
	 */

	@Override
	public String execute(HttpServletRequest request) {
		int productId = Integer.parseInt(request.getParameter("product_id"));

		Product product = ProductService.getInstance().findProductById(productId);
		if (product == null) {
			request.setAttribute("message", MessageKey.FIND_PRODUCT_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			request.setAttribute("product", product);
			return configurationManager.getProperty("path.page.product");
		}

	}

}
