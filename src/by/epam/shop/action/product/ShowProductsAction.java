package by.epam.shop.action.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.service.product.ProductService;

public class ShowProductsAction implements Action {

	/**
	 * The method for showing description of a group of products from the
	 * database. Products groups by type.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		String type = request.getParameter("product_type");
		List<Product> products = ProductService.getInstance().findProductsByType(type);
		if (products.isEmpty()) {
			request.setAttribute("message", MessageKey.FIND_PRODUCTS_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			request.setAttribute("products", products);
			return configurationManager.getProperty("path.page.products");
		}

	}

}
