package by.epam.shop.action.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.service.product.ProductService;

public class AddProductAction implements Action {

	/**
	 * The method for initialization and adding a new product to the database
	 * from the web-page. Allowed only for an administrator.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		String type = request.getParameter("type");
		String picturePath = request.getParameter("picture");
		String producer = request.getParameter("producer");
		String model = request.getParameter("model");
		String size = request.getParameter("size");
		String pcd = request.getParameter("pcd");
		String dia = request.getParameter("dia");
		String et = request.getParameter("et");
		String description = request.getParameter("description");
		String price = request.getParameter("price");
		ProductService productService = ProductService.getInstance();

		if (type.length() * picturePath.length() * producer.length() * model.length() * size.length() * pcd.length()
				* dia.length() * et.length() * description.length() * price.length() == 0) {
			List<String> types = productService.findAllProductTypes();
			List<String> pictures = productService.findAllProductPicturePath();
			request.setAttribute("types", types);
			request.setAttribute("picturePath", pictures);
			request.setAttribute("message", MessageKey.ADD_PRODUCT_BLANK_FIELDS);
			return configurationManager.getProperty("path.page.addproduct");
		}
		Product product = productService.initProductForAdd(type, picturePath, producer, model, size, pcd, dia, et,
				description, price);
		String validationMessage = ProductService.validateProduct(product);

		if (validationMessage != null) {
			request.setAttribute("message", validationMessage);
			return configurationManager.getProperty("path.page.addproduct");
		}
		if (productService.addProduct(product)) {
			request.setAttribute("message", MessageKey.ADD_PRODUCT_SUCCESS);
			return configurationManager.getProperty("path.page.success");
		} else {
			request.setAttribute("message", MessageKey.ADD_PRODUCT_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}
}
