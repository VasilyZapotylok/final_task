package by.epam.shop.action.product;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.User;
import by.epam.shop.service.product.ProductService;

public class DeleteProductAction implements Action {

	/**
	 * The method for deleting a concrete product from the database. Allowed
	 * only for an administrator.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int productId = Integer.parseInt(request.getParameter("product_id"));
		if (ProductService.getInstance().deleteProduct(productId)) {
			request.setAttribute("message", MessageKey.DELETE_PRODUCT_SUCCESS);
			return configurationManager.getProperty("path.page.success");
		} else {
			request.setAttribute("message", MessageKey.DELETE_PRODUCT_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}

}
