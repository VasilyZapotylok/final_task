package by.epam.shop.action.order;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Order;
import by.epam.shop.service.order.OrderService;

public class ShowConcreteOrderAction implements Action {

	/**
	 * The method for showing concrete order from the database.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		int orderId = Integer.parseInt(request.getParameter("order_id"));
		OrderService orderService = OrderService.getInstance();
		Order order = orderService.findOrderById(orderId);
		if (order == null) {
			request.setAttribute("message", MessageKey.FIND_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			int price = orderService.countPrice(order);
			request.setAttribute("products", order.getProducts());
			request.setAttribute("full_price", price);
			request.setAttribute("order_id", order.getId());
			return configurationManager.getProperty("path.page.order");
		}
	}
}
