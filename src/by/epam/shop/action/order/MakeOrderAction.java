package by.epam.shop.action.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.service.order.OrderService;

public class MakeOrderAction implements Action {
	
	/**
	 * The method for making order. The method checks user's shopping cart and
	 * removes products in the order.
	 */
	
	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		String comment = request.getParameter("user_comment");
		if (user == null) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (user.getBlackListFlag() == 1) {
			request.setAttribute("message", MessageKey.BUY_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		List<Product> products = user.getShoppingCart();
		if (products == null) {
			request.setAttribute("message", MessageKey.MAKE_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		OrderService orderService = OrderService.getInstance();
		Order order = orderService.initOrderForMakeAction(products, user, comment);
		if (orderService.createOrder(order)) {
			user.removeAllProducts();
			request.setAttribute("message", MessageKey.MAKE_ORDER_SUCCESS);
			return configurationManager.getProperty("path.page.success");
		} else {
			request.setAttribute("message", MessageKey.MAKE_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}

}
