package by.epam.shop.action.order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.User;
import by.epam.shop.service.order.OrderService;

public class ShowOrdersAction implements Action {

	/**
	 * The method for showing all orders of a concrete user.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		List<Order> orders = OrderService.getInstance().findOrdersById(user.getId());
		if (orders == null) {
			request.setAttribute("message", MessageKey.DATABASE_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else if (orders.isEmpty()) {
			request.setAttribute("message", MessageKey.SHOW_ORDERS_ERROR);
			return configurationManager.getProperty("path.page.error");
		} else {
			request.setAttribute("orders", orders);
			return configurationManager.getProperty("path.page.orders");
		}

	}
}
