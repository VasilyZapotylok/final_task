package by.epam.shop.action.order;

import javax.servlet.http.HttpServletRequest;

import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.constant.OrderStatus;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.User;
import by.epam.shop.service.order.OrderService;

public class DeliverOrderAction implements Action {

	/**
	 * The method changes orders's status to DELIVERED if it's ACTIVE.
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int orderId = Integer.parseInt(request.getParameter("order_id"));
		OrderService orderService = OrderService.getInstance();
		Order order = orderService.findOrderById(orderId);
		if (order == null) {
			request.setAttribute("message", MessageKey.FIND_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (OrderStatus.ACTIVE.equals(order.getStatus())) {
			order.setStatus(OrderStatus.DELIVERED);
			if (orderService.updateStatus(order)) {
				request.setAttribute("message", MessageKey.DELIVER_ORDER_SUCCESS);
				return configurationManager.getProperty("path.page.success");
			}
		}
		request.setAttribute("message", MessageKey.DELIVER_ORDER_ERROR);
		return configurationManager.getProperty("path.page.error");

	}
}
