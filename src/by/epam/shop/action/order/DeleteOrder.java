package by.epam.shop.action.order;

import javax.servlet.http.HttpServletRequest;


import by.epam.shop.action.Action;
import by.epam.shop.constant.MessageKey;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.User;
import by.epam.shop.service.order.OrderService;

public class DeleteOrder implements Action {
	
	/** 
	 * The method for deleting a concrete order. Uses only for administrator. 
	 */

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null || user.getAccessLevel() != 2) {
			request.setAttribute("message", MessageKey.USER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		int orderId = Integer.parseInt(request.getParameter("order_id"));
		OrderService orderService = OrderService.getInstance();
		Order order = orderService.findOrderById(orderId);
		if (order == null) {
			request.setAttribute("message", MessageKey.FIND_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}
		if (orderService.deleteOrder(order)) {
			request.setAttribute("message", MessageKey.DELETE_ORDER_SUCCESS);
			return configurationManager.getProperty("path.page.success");
		} else {
			request.setAttribute("message", MessageKey.DELETE_ORDER_ERROR);
			return configurationManager.getProperty("path.page.error");
		}

	}

}
