package by.epam.shop.entity;

import java.io.Serializable;
import java.util.ArrayList;


public class User extends AbstractEntity implements Serializable {
	

	private static final long serialVersionUID = -3754563345344300086L;
	private String login;
	private String password;
	private String phone;
	private int blackListFlag;
	private int accessLevel;
	private ArrayList<Product> shoppingCart = new ArrayList<>();

	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getBlackListFlag() {
		return blackListFlag;
	}

	public void setBlackListFlag(int blackListFlag) {
		this.blackListFlag = blackListFlag;
	}

	public int getAccessLevel() {
		return accessLevel;
	}


	public void setAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	public ArrayList<Product> getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ArrayList<Product> shoppingCart) {
		this.shoppingCart = shoppingCart;
	}


	public void addProduct(Product product) {
		shoppingCart.add(product);
	}

	public void removeProduct(Product product) {
		shoppingCart.remove(product);
	}

	
	public void removeAllProducts() {
		shoppingCart.removeAll(shoppingCart);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + accessLevel;
		result = prime * result + blackListFlag;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result
				+ ((shoppingCart == null) ? 0 : shoppingCart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (accessLevel != other.accessLevel)
			return false;
		if (blackListFlag != other.blackListFlag)
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (shoppingCart == null) {
			if (other.shoppingCart != null)
				return false;
		} else if (!shoppingCart.equals(other.shoppingCart))
			return false;
		return true;
	}


}
