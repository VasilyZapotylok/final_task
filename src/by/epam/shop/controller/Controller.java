package by.epam.shop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.shop.action.Action;
import by.epam.shop.action.ActionFactory;

/**
 * The class Controller receives data from a request, performs concrete action
 * and forwards to the needed page.
 */

@WebServlet("/Controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = -7292895805164842443L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		ActionFactory factory = ActionFactory.getInstance();
		Action action = factory.defineCommand(request);
		if (action != null) {
			page = action.execute(request);
			RequestDispatcher rd = request.getRequestDispatcher(page);
			rd.forward(request, response);
		} else {
			response.sendError(500);
		}

	}
}
