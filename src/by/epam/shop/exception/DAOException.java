package by.epam.shop.exception;

/**
 * Custom exception.
 */

public class DAOException extends Exception {

	private static final long serialVersionUID = -7124666033055001758L;

	public DAOException() {
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}
}
