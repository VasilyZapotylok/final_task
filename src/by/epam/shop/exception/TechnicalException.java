package by.epam.shop.exception;

/**
 * Custom exception.
 */

public class TechnicalException extends Exception {

	private static final long serialVersionUID = -7550785983868985402L;

	public TechnicalException() {
	}

	public TechnicalException(String message, Throwable cause) {
		super(message, cause);
	}

	public TechnicalException(String message) {
		super(message);
	}

	public TechnicalException(Throwable cause) {
		super(cause);
	}
}
