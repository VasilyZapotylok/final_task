package by.epam.shop.service.order;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.shop.constant.OrderStatus;
import by.epam.shop.dao.OrderDAO;
import by.epam.shop.entity.Order;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;

/**
 * The class OrderService contains utility methods for accessing order's data
 * and logic.
 */

public class OrderService {
	private static final Logger LOGGER = Logger.getLogger(OrderService.class);

	public List<Order> findAllOrders() {
		List<Order> orders = null;
		try {
			orders = OrderDAO.getInstance().findAll();
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return orders;
	}

	public List<Order> findOrdersById(int id) {
		List<Order> orders = null;
		try {
			orders = OrderDAO.getInstance().findEntitiesByUserId(id);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return orders;
	}

	public Order findOrderById(int id) {
		Order order = null;
		try {
			order = OrderDAO.getInstance().findEntityById(id);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return order;
	}

	/**
	 * The method counts price of a concrete order.
	 */
	public int countPrice(Order order) {
		int price = 0;
		for (Product product : order.getProducts()) {
			price += product.getPrice();
		}
		return price;
	}

	/**
	 * The method initializes an order from the data from web-pages. Default
	 * status - ACTIVE.
	 */
	public Order initOrderForMakeAction(List<Product> products, User user, String comment) {
		Order order = new Order();
		order.setProducts(products);
		order.setStatus(OrderStatus.ACTIVE);
		order.setUser(user);
		order.setComment(comment);
		return order;
	}

	public boolean createOrder(Order order) {
		boolean isDone = false;
		try {
			isDone = OrderDAO.getInstance().create(order);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}

	public boolean updateStatus(Order order) {
		boolean isDone = false;
		try {
			isDone = OrderDAO.getInstance().updateStatus(order);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}

	public boolean deleteOrder(Order order) {
		boolean isDone = false;
		try {
			isDone = OrderDAO.getInstance().delete(order);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}

	public static OrderService getInstance() {
		return new OrderService();
	}

}
