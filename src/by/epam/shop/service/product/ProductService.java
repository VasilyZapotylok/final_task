package by.epam.shop.service.product;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.epam.shop.constant.MessageKey;
import by.epam.shop.dao.ProductDAO;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;

/**
 * The class ProductService contains utility methods for accessing product's
 * data and logic.
 */

public class ProductService {
	private static final Logger LOGGER = Logger.getLogger(ProductService.class);

	/**
	 * The method checks main parameters of a concrete product used accepted
	 * rules. If parameters are OK the method returns null, else returns message
	 * contains name of wrong parameter.
	 */
	public static String validateProduct(Product product) {

		final Pattern PRODUCT_PRODUCER_PATTERN = Pattern.compile("^[\\w\\s-&]{1,45}$");
		final Pattern PRODUCT_MODEL_PATTERN = Pattern.compile("^[\\w\\s-&.]{1,45}$");
		final Pattern PRODUCT_SIZE_PATTERN = Pattern.compile("^R[1-9]{2}x[1-9]{1}[\\.]?[0-9]?$");
		final Pattern PRODUCT_PCD_PATTERN = Pattern.compile("^[3-9]{1}x[0-9]{3}$");
		final Pattern PRODUCT_DIA_PATTERN = Pattern.compile("^[1-9]{2}(\\.\\d{1,2})?$");
		final Pattern PRODUCT_ET_PATTERN = Pattern.compile("^[0-9]{2}$");
		final Pattern PRODUCT_PRICE_PATTERN = Pattern.compile("^[0-9]{6,7}$");

		if (!PRODUCT_PRODUCER_PATTERN.matcher(product.getProducer()).matches()) {
			return MessageKey.PRODUCT_PRODUCER_PATTERN_ERROR;
		}
		if (!PRODUCT_MODEL_PATTERN.matcher(product.getModel()).matches()) {
			return MessageKey.PRODUCT_MODEL_PATTERN_ERROR;
		}
		if (!PRODUCT_SIZE_PATTERN.matcher(product.getSize()).matches()) {
			return MessageKey.PRODUCT_SIZE_PATTERN_ERROR;
		}
		if (!PRODUCT_PCD_PATTERN.matcher(product.getPcd()).matches()) {
			return MessageKey.PRODUCT_PCD_PATTERN_ERROR;
		}
		if (!PRODUCT_DIA_PATTERN.matcher(String.valueOf(product.getDia())).matches()) {
			return MessageKey.PRODUCT_DIA_PATTERN_ERROR;
		}
		if (!PRODUCT_ET_PATTERN.matcher(String.valueOf(product.getEt())).matches()) {
			return MessageKey.PRODUCT_ET_PATTERN_ERROR;
		}
		if (!PRODUCT_PRICE_PATTERN.matcher(String.valueOf(product.getPrice())).matches()) {
			return MessageKey.PRODUCT_PRICE_PATTERN_ERROR;
		}
		return null;
	}

	public static ProductService getInstance() {
		return new ProductService();
	}

	public Product findProductById(int id) {
		Product product = null;
		try {
			product = ProductDAO.getInstance().findEntityById(id);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return product;
	}

	public List<Product> findProductsByType(String type) {
		List<Product> products = null;

		try {
			products = ProductDAO.getInstance().findEntitiesByType(type);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return products;
	}
/**
 * Search products by main parameters: SIZE, PCD, DIA, ET. 
 */
	public List<Product> searchProducts(String text) {

		Pattern textPattern = Pattern.compile(text, Pattern.CASE_INSENSITIVE);

		List<Product> products = findAllProducts();
		List<Product> findProducts = new ArrayList<Product>();
		for (Product product : products) {
			if (textPattern.matcher(product.getSize()).find()) {
				findProducts.add(product);
			}
			if (textPattern.matcher(product.getPcd()).find()) {
				findProducts.add(product);
			}
			if (textPattern.matcher(String.valueOf(product.getDia())).find()) {
				findProducts.add(product);
			}
			if (textPattern.matcher(String.valueOf(product.getEt())).find()) {
				findProducts.add(product);
			}
		}
		return findProducts;
	}

	public List<Product> findAllProducts() {
		List<Product> products = null;
		try {
			products = ProductDAO.getInstance().findAll();
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return products;
	}

	public List<String> findAllProductTypes() {
		List<String> types = null;
		try {
			types = ProductDAO.getInstance().findAllProductTypes();
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return types;
	}

	public List<String> findAllProductPicturePath() {
		List<String> pictures = null;
		try {
			pictures = ProductDAO.getInstance().findAllProductPicturePath();
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return pictures;
	}
/**
 * Initialization of a concrete product from data from a web-page.
 */
	public Product initProduct(String id, String type, String picturePath, String producer, String model, String size,
			String pcd, String dia, String et, String description, String price) {
		Product product = new Product();

		product.setId(Integer.parseInt(id));
		product.setType(type);
		product.setPicturePath(picturePath);
		product.setProducer(producer);
		product.setModel(model);
		product.setSize(size);
		product.setPcd(pcd);
		product.setDia(Double.parseDouble(dia));
		product.setEt(Integer.parseInt(et));
		product.setDescription(description);
		product.setPrice(Integer.parseInt(price));

		return product;
	}

	public Product updateProduct(Product product) {
		Product prod = null;
		try {
			prod = ProductDAO.getInstance().update(product);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return prod;
	}

	public boolean deleteProduct(int id) {
		boolean isDone = false;
		try {
			isDone = ProductDAO.getInstance().delete(id);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}

	public boolean addProduct(Product product) {
		boolean isDone = false;
		try {
			isDone = ProductDAO.getInstance().create(product);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}
	/**
	 * Initialization of a concrete product from data from a web-page.
	 */
	public Product initProductForAdd(String type, String picturePath, String producer, String model, String size,
			String pcd, String dia, String et, String description, String price) {
		Product product = new Product();
		product.setType(type);
		product.setPicturePath(picturePath);
		product.setProducer(producer);
		product.setModel(model);
		product.setSize(size);
		product.setPcd(pcd);
		product.setDia(Double.parseDouble(dia));
		product.setEt(Integer.parseInt(et));
		product.setDescription(description);
		product.setPrice(Integer.parseInt(price));

		return product;
	}
/**
 * Sorting list of products for find new products in the database. Uses IDs in the database.
 */
	public List<Product> sortListForLastProducts(List<Product> products) {
		int maxIndex = 0;
		for (Product product : products) {
			if (maxIndex < product.getId()) {
				maxIndex = product.getId();
			}
		}
		Iterator<Product> iterator = products.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getId() < maxIndex - 4) {
				iterator.remove();
			}
		}
		return products;
	}
/**
 * Checking amount of products in the concrete order. Accepted value at least 1 up to 10. 
 */
	public User checkAmount(User user, Product product, int amount) {
		if (amount > 0 & amount < 11) {
			for (int i = 0; i < amount; i++) {
				user.addProduct(product);
			}
		}
		return user;

	}

}
