package by.epam.shop.service.user;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import by.epam.shop.constant.MessageKey;
import by.epam.shop.dao.UserDAO;
import by.epam.shop.entity.Product;
import by.epam.shop.entity.User;
import by.epam.shop.exception.DAOException;

/**
 * The class UserService contains utility methods for accessing user's data and
 * logic.
 */
public class UserService {
	private static final Logger LOGGER = Logger.getLogger(UserService.class);

	/**
	 * The method for coding String value.
	 */
	public static String hashMD5(String text) {
		return DigestUtils.md5Hex(text);
	}

	/**
	 * The method checks main parameters of a concrete user used accepted rules.
	 * If parameters are OK the method returns null, else returns message
	 * contains name of wrong parameter.
	 */
	public static String validateUser(User user) {

		final Pattern LOGIN_PATTERN = Pattern.compile("^[\\w]{5,15}$");
		final Pattern PASSWORD_PATTERN = Pattern.compile("^[\\w]{5,15}$");
		final Pattern PHONE_PATTERN = Pattern.compile("^\\+375[0-9]{9}$");

		if (!LOGIN_PATTERN.matcher(user.getLogin()).matches()) {
			return MessageKey.REGISTER_LOGIN_PATTERN_ERROR;
		}
		if (!PASSWORD_PATTERN.matcher(user.getPassword()).matches()) {
			return MessageKey.REGISTER_PASSWORD_PATTERN_ERROR;
		}
		if (!PHONE_PATTERN.matcher(user.getPhone()).matches()) {
			return MessageKey.REGISTER_PHONE_PATTERN_ERROR;
		}
		return null;
	}

	/**
	 * The method counts full price of all products in the shopping cart.
	 */
	public int countFullPrice(User user) {
		int fullPrice = 0;
		for (Product product : user.getShoppingCart()) {
			fullPrice += product.getPrice();
		}
		return fullPrice;
	}

	/**
	 * The method for deleting a concrete product from a shopping cart.
	 */
	public User deleteProductFromCart(User user, int id) {
		Iterator<Product> iterator = user.getShoppingCart().iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getId() == id) {
				iterator.remove();
				break;
			}
		}
		return user;
	}

	/**
	 * The method for initialization a user from data from a web-page for add to
	 * the database. Default access level - user - flag 1.
	 */
	public User initUserForRegister(String login, String password, String phone) {
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		user.setPhone(phone);
		user.setAccessLevel(1);
		return user;
	}

	public User findUserByLogin(String login) {
		User user = null;
		try {
			user = UserDAO.getInstance().findEntityByLogin(login);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return user;
	}

	public boolean addUser(User user) {
		boolean isDone = false;
		try {
			isDone = UserDAO.getInstance().create(user);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return isDone;
	}

	public User findUserByLoginAndPassword(String login, String password) {
		User user = null;
		try {
			user = UserDAO.getInstance().findEntityByLoginAndPassword(login, password);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return user;
	}

	public User findUserById(int userId) {
		User user = null;

		try {
			user = UserDAO.getInstance().findEntityById(userId);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return user;
	}

	public void updateUser(User user) {
		try {
			UserDAO.getInstance().update(user);
		} catch (DAOException e) {
			LOGGER.error(e);
		}
	}

	public List<User> findAllUsers() {
		List<User> users = null;
		try {
			users = UserDAO.getInstance().findAll();
		} catch (DAOException e) {
			LOGGER.error(e);
		}
		return users;
	}

	public static UserService getInstance() {
		return new UserService();
	}
}
