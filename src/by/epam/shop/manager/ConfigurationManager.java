package by.epam.shop.manager;

import java.util.ResourceBundle;

/**
 * ConfigurationManager for getting access to the property-file with general paths.
 */

public class ConfigurationManager {
	private static final String PROPERTY_FILE = "config";
	private ResourceBundle resourceBundle;
	private static ConfigurationManager instance = new ConfigurationManager();

	private ConfigurationManager() {
		resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);
	}

	public static ConfigurationManager getInstance() {
		return instance;
	}

	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
