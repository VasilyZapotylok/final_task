<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="title">
		<fmt:message key="addproduct.title" />
	</div>
	<div style="width: 300px; padding-left: 30%; padding-top: 50px">
		<c:if test="${not empty message }">
			<div style="margin-bottom: 10px; color: red; font-size: 18px">
				<fmt:message key="${message }" />
			</div>
		</c:if>
		<form method="POST" action="Controller">
			<div>
				<input name="action" type="hidden" value="add_product" />
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">

				<fmt:message key="message.required" />
				<fmt:message key="product.type" />
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 12px">
				<select name="type"><c:forEach items="${types}" var="type">
						<option>${type }</option>
					</c:forEach></select>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">

				<fmt:message key="message.required" />
				<fmt:message key="product.image" />
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 12px">
				<select name="picture"><c:forEach items="${picturePath}"
						var="path">
						<option>${path }</option>
					</c:forEach></select>
			</div>

			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">

				<fmt:message key="message.required" />
				<fmt:message key="product.producer" />
			</div>
			<div>
				<input id="regInput" type="text" name="producer"
					pattern="^[\w\s-&]{1,45}$" required value="${product.producer }" />
			</div>

			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.producer" />
				</div>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">

				<fmt:message key="message.required" />
				<fmt:message key="product.model" />
			</div>
			<div>
				<input id="regInput" type="text" name="model"
					pattern="^[\w\s-&.]{1,45}$" required value="${product.model }" />
			</div>
			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.model" />
				</div>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.size" />
			</div>
			<div>
				<input id="regInput" type="text" name="size"
					pattern="^R[1-9]{2}x[1-9]{1}[\.]?[0-9]?$" required
					value="${product.size }" />
			</div>

			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.size" />
				</div>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.pcd" />
			</div>
			<div>
				<input id="regInput" type="text" name="pcd"
					pattern="^[3-9]{1}x[0-9]{3}$" required value="${product.pcd }" />
			</div>

			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.pcd" />
				</div>
			</div>

			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.dia" />
			</div>
			<div>
				<input id="regInput" type="text" name="dia"
					pattern="^[1-9]{2}(\.\d{1,2})?$" required value="${product.dia }" />
			</div>

			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.dia" />
				</div>
			</div>

			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.et" />
			</div>
			<div>
				<input id="regInput" type="text" name="et" pattern="^[0-9]{2}$"
					required value="${product.et }" />
			</div>
			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.et" />
				</div>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.description" />
			</div>
			<div>
				<textarea id="orderText" rows="10" cols="40" name="description"
					required>${product.description }</textarea>
			</div>
			<div style="font-size: 18px; margin-top: 12px; margin-bottom: 5px">
				<fmt:message key="message.required" />
				<fmt:message key="product.price" />
			</div>
			<div>
				<input id="regInput" type="text" name="price" pattern="^[0-9]{6,7}$"
					required value="${product.price }" />
			</div>
			<div>
				<div style="font-size: 14px">
					<fmt:message key="addproduct.info.price" />
				</div>
			</div>

			<div style="margin-top: 16px; padding-bottom: 20px">
				<input id="regButton" type="submit"
					value="<fmt:message key="addproduct.button" />" />
			</div>
		</form>
	</div>
</body>
</html>