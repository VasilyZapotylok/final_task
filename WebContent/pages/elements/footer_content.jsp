<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div id ="footer">
		<form name="submitFormFooter" action="Controller" method="POST">
						<input name="action" type="hidden" value="go_to_page" /> <input
							name="page" type="hidden" value="path.page.about" /> <a
							href="javascript:document.submitFormFooter.submit()"> <fmt:message
				key="footer.about" /></a>
					</form>
	</div>
</body>
</html>