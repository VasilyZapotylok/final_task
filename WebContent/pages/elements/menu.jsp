<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<ul>
		<li><form action="Controller" method="POST">
				<input name="action" type="hidden" value="show_last_products" />  <input
					class="button" type="submit"
					value="<fmt:message key="menu.button.main"/>" />
			</form></li>
		<li><form action="Controller" method="POST">
				<input name="action" type="hidden" value="show_products" /> <input
					name="product_type" type="hidden" value="Литой" /> <input
					class="button" type="submit"
					value="<fmt:message key="menu.button.alloy"/>" />
			</form></li>
		<li><form action="Controller" method="POST">
				<input name="action" type="hidden" value="show_products" /> <input
					name="product_type" type="hidden" value="Кованный" /> <input
					class="button" type="submit"
					value="<fmt:message key="menu.button.forged"/>" />
			</form></li>
		<li><form action="Controller" method="POST">
				<input name="action" type="hidden" value="show_products" /> <input
					name="product_type" type="hidden" value="Стальной" /> <input
					class="button" type="submit"
					value="<fmt:message key="menu.button.steel"/>" />
			</form></li>
	</ul>
</body>
</html>