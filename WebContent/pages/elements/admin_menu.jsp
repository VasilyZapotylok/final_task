<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<ul>
	<li><form action="Controller" method="POST">
					<input name="action" type="hidden" value="add_product_page" /> <input
						class="button" type="submit"
						value="<fmt:message key="menu.button.addproduct"/>" />
				</form></li>
			<li><form action="Controller" method="POST">
					<input name="action" type="hidden" value="all_orders_page" /> <input
						class="button" type="submit"
						value="<fmt:message key="menu.button.allorders"/>" />
				</form></li>
			<li><form action="Controller" method="POST">
					<input name="action" type="hidden" value="users_page" /><input
						class="button" type="submit"
						value="<fmt:message key="menu.button.users"/>" />
				</form></li>
	</ul>
</body>
</html>