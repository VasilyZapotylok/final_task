<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
	<form id="sendError" name="sendError"
		action="${pageContext.request.contextPath}/Controller" method="POST">
		<input name="action" type="hidden" value="go_to_page" /> <input
			name="page" type="hidden" value="path.page.error" /> <input
			name="code" type="hidden" value="${pageContext.errorData.statusCode}" />
	</form>
	<script type="text/javascript">
		document.getElementById("sendError").submit();
	</script>
</body>
</html>