<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:if test="${sessionScope.locale == 'ru' or empty sessionScope.locale}">
	<fmt:setLocale value="ru_RU" scope="session" />
</c:if>
<c:if test="${sessionScope.locale == 'en'}">
	<fmt:setLocale value="en_US" scope="session" />
</c:if>
<fmt:setBundle basename="locale" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link rel="shortcut icon" href="images/favicon.png" />
<title><fmt:message key="title.about" /></title>
</head>
<body>
	<div>
		<%@include file="/pages/elements/header.jsp"%>
	</div>
	<div id="main">
		<div id="left-content">
			<div id="login">
				<%@include file="/pages/elements/login.jsp"%>
			</div>
				<c:if test="${sessionScope.user.accessLevel == 2 }">
				<div id="menu">
				<%@include file="/pages/elements/admin_menu.jsp"%>
			</div>
			</c:if>
			<div id="menu">
				<%@include file="/pages/elements/menu.jsp"%>
			</div>
		</div>
		<div id="right-content">
			<div id="content">
				<div
					style="width: 100%; text-align: center; font-size: 20px; padding-top: 40px">
					<fmt:message key="about.info" />
				</div>
			</div>
		</div>
		<div style="clear: left"></div>
	</div>
	<div>
	<%@include file="/pages/elements/footer_content.jsp"%>
	</div>
</body>
</html>