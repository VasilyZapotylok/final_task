package by.epam.shop.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import by.epam.shop.connectionpool.ConnectionPoolTest;
import by.epam.shop.service.order.CountPriceOrderTest;
import by.epam.shop.service.user.UserValidatorTest;

@RunWith(Suite.class)
@SuiteClasses({ ConnectionPoolTest.class, UserValidatorTest.class, CountPriceOrderTest.class })
public class AllTests {

}
