package by.epam.shop.connectionpool;

import by.epam.shop.connectionpool.ConnectionPool;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConnectionPoolTest {
	private  ConnectionPool connectionPool;
	
	@Before
	public  void initConnectionPool(){
		connectionPool = ConnectionPool.getInstance();
	}  

	@Test
	public void testConnectionPool() {
		for (int i = 0; i < 20; i++) {
			Assert.assertNotNull(connectionPool.getConnection());
			
		}
	}
	
	@After
	public  void shutDownConnectionPool(){
		connectionPool.shutDown();
	}

}
