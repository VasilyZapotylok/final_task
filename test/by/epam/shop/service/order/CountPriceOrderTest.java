package by.epam.shop.service.order;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.epam.shop.entity.Order;
import by.epam.shop.entity.Product;

public class CountPriceOrderTest {
	private Product product;
	private Order order;
	List<Product> products;

	@Before
	public void setUp() {
		product = new Product();
		product.setPrice(100);
		List<Product> products = new ArrayList<Product>();
		for (int i = 0; i < 10; i++) {
			products.add(product);
		}
		order = new Order();
		order.setProducts(products);
	}

	@Test
	public void test() {
		int price = OrderService.getInstance().countPrice(order);
		assertEquals(price, 1000);
	}

	@After
	public void setDown() {
		product = null;
		products = null;
		order = null;
	}
}
