package by.epam.shop.service.user;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import by.epam.shop.entity.User;

@RunWith(Parameterized.class)
public class UserValidatorTest {
	private User user;
	private String login;
	private String password;
	private String phone;

	public UserValidatorTest(String login, String password, String phone) {
		this.login = login;
		this.password = password;
		this.phone = phone;
	}

	@Before
	public void setUp() throws Exception {
		user = new User();
		user.setLogin(login);
		user.setPassword(password);
		user.setPhone(phone);
	}

	@Parameters
	public static Collection<String[]> stepUpStringvalues() {
		return Arrays.asList(new String[][] { { "admin", "admin", "+375297781341" },
				{ "user1", "user1", "+375447786219" }, { "user2", "user2", "+375447786219" } });
	}

	@Test
	public void validationTest() {
		assertNull(UserService.validateUser(user));
	}

	@After
	public void setDown() throws Exception {
		user = null;
	}
}
